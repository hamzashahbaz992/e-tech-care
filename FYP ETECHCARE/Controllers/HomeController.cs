﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.IO;
namespace ETechCare.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult facebook()
        {
            return Redirect("https://www.google.com.pk/");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        //my code
        public ActionResult etechcare()
        {
            return View();
        }
        public ActionResult services()
        {
            if (Session["user_id"] == null)
            {
                return Redirect("~/Home/etechcare");
            }
            return View();
        }
        public ActionResult docprofile()
        {
            if (Session["doctor_id"] != null)
            {
                int id = Convert.ToInt32(Session["doctor_id"]);
                Models.UserDTO user = Models.User.getUserByID(id);
                String time = user.time;
                int i = 1;
                String t1 =  "";
                String a1 = "";
                String t2 = "";
                String a2 = "";
                foreach(char t in time)
                {
                    if (i <= 5)
                    {
                        t1 = t1 + t;
                    }
                    else if (i > 5 && i <= 7)
                    {
                        a1 = a1 + t;
                    }
                    else if (i >=12 && i <= 16)
                    {
                        t2 = t2 + t;
                    }
                    else if (i >= 17 && i <= 18)
                    {
                        a2 = a2 + t;
                    }
                    i = i + 1;
                }
                ViewBag.t1 = t1;
                ViewBag.a1 = a1;
                ViewBag.t2 = t2;
                ViewBag.a2 = a2;
                return View(user);
            }
            return Redirect("~/Home/etechcare");
        }
        public ActionResult docinfo()
        {
            if (Session["doctor_id"] == null)
           {
                return Redirect("~/Home/etechcare");
           }
            return View();
        }
        
        public ActionResult terms_and_conditions()
        {
            return View();
        }
        [HttpPost]
        public ActionResult signin()
        {
            string email = Request["email"];
            string password = Request["password"];
            if (Models.User.login(email, password) == 1)
            {
                int id = Models.User.getID(email);
                Session["user_id"] = id;
                return Redirect("~/Home/services");

            }
            else if (Models.User.login(email,password) == 2)
            {
                int id = Models.User.getID(email);
                Session["doctor_id"] = id;
                Models.UserDTO user = Models.User.getUserByID(id);
                int x = 0;
                return Redirect("~/Home/docprofile");
            }
            Session["flogin"] = "failed";
            return Redirect("~/Home/etechcare");
        }

        [HttpPost]
        public ActionResult etechcare(Models.UserDTO user)
        {
            if (Models.User.signUp(user) == false)
            {
                ViewBag.Exist = "Email Exists";
                return View();
            }
            if (user.signupas == "Doctor")
            {
                int id = Models.User.getID(user.email);
                Session["doctor_id"] = id;
                return Redirect("~/Home/docinfo");
            }
            else if (user.signupas == "Patient")
            {
                int id = Models.User.getID(user.email);
                Session["user_id"] = id;
                return Redirect("~/Home/services");
            }
            return View();
        }

        [HttpPost]
        public ActionResult addinfo(string email,string fee,string number,string time1,string time2,string hospital,string address,string ap1,string ap2,string specialization)
        {
            /*try
            {
                int p = Convert.ToInt32(number);
                int f = Convert.ToInt32(fee);
            }
            catch(Exception e)
            {
                Session["invalid"] = "invalid";
                return Redirect("~/Home/docinfo");
            }*/
            string description =  email + "|" +  fee + "|"  + hospital + "|" +  address + "|" +  time1 + ap1 + " to " + time2 + ap2 + "|" + number;
            Models.User.addInfo(description, Convert.ToInt32(Session["doctor_id"]));
            int spid = Convert.ToInt32(specialization);
            int did = Convert.ToInt32(Session["doctor_id"]);
            Models.User.addDoctorSpecialization(did, spid);
            return Redirect("~/Home/docprofile");
        }
        [HttpPost]
        public ActionResult updateInfo(string email, string fee, string number, string time1, string time2, string hospital, string address, string ap1, string ap2, string specialization,string name,string password,string city)
        {
           /* try
            {
                int p = Convert.ToInt32(number);
                int f = Convert.ToInt32(fee);
            }
            catch (Exception e)
            {
                Session["invalid"] = "invalid";
                return Redirect("~/Home/docprofile");
            }*/
            string description = email + "|" + fee + "|" + hospital + "|" + address + "|" + time1 + ap1 + " to " + time2 + ap2 + "|" + number;
            Models.User.updateInfo(description, Convert.ToInt32(Session["doctor_id"]),name,password,city);
            int spid = Convert.ToInt32(specialization);
            int did = Convert.ToInt32(Session["doctor_id"]);
            Models.User.addDoctorSpecialization(did, spid);
            return Redirect("~/Home/docprofile");
        }
        public ActionResult showDoctor(int id)
        {
            if (Session["user_id"] == null)
            {
                return Redirect("~/Home/etechcare");
            }
            Models.UserDTO user = Models.User.getUserByID(id);

            return View(user);
        }
        public ActionResult aboutus()
        {
            return View();
        }
        public ActionResult contactus()
        {
            return View();
        }
      
        public ActionResult logout()
        {
            Session.RemoveAll();
            return Redirect("~/Home/etechcare");
            
        }
        [HttpPost]
        public ActionResult uploadpic(HttpPostedFileBase profilepic)
        {
            string newprofilepic = Request["profilepic"];
            string path = "";
            string newimagename = "";

            if (Path.GetExtension(profilepic.FileName).ToLower() == ".jpg")
            {
                newimagename = Guid.NewGuid().ToString() + ".jpg";
                path = Path.Combine(Server.MapPath("~/Profiles"), newimagename);
                profilepic.SaveAs(path);
            }

            int id = Convert.ToInt32(Session["doctor_id"]);
            Models.User.uploadpic(newimagename, id);
            
            return Redirect("~/Home/docprofile");
        }
        public ActionResult forgot()
        {
            return View();
        }
        [HttpPost]
        public ActionResult reset(string email)
        {
            if (Models.User.mail(email) == false)
            {
                Session["email"] = "not sent";
            }
            else
            {
                Session["emails"] = "sent";
            }
            return Redirect("~/Home/etechcare");
        }
        
    }
}