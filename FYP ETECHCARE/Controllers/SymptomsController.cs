﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace ETechCare.Controllers
{
    public class SymptomsController : Controller
    {
        //
        // GET: /Symptoms/
        public ActionResult symptoms()
        {
            if (Session["user_id"] == null)
            {
                return Redirect("~/Home/etechcare");
            }
            return View();
        }
        public ActionResult diseaseName()
        {

            if (Session["user_id"] == null)
            {
                return Redirect("~/Home/etechcare");
            }
            return View();
        }
        [HttpPost]
        [ActionName("symptoms")]
        public ActionResult symptoms(string agilation)
        {
            int [] symptomListDetector = new int[90];
            int count = 0;
            for (int i = 0;i < 90;i++)
            {
                symptomListDetector[i] = 0;
            }
            //1
            if (Request["abnormaldischarge"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["abnormaldischarge"])] = 1;
            }
            //2
            if (Request["agilation"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["agilation"])] = 1;
            }
            //3
            if (Request["anxiety"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["anxiety"])] = 1;
            }
            //4
            if (Request["bleeding"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["bleeding"])] = 1;
            }
            //5
            if (Request["stools"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["stools"])] = 1;
            }
            //6
            if (Request["burine"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["burine"])] = 1;
            }
            //7
            if (Request["breathlessness"] != null)
            {
                symptomListDetector[Convert.ToInt32(Request["breathlessness"])] = 1;
            }
            //8
            if (Request["paleskin"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["paleskin"])] = 1;
            }
            //9
            if (Request["blueskin"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["blueskin"])] = 1;
            }
            //10
            if (Request["yellowskin"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["yellowskin"])] = 1;
            }
            //11
            if (Request["redskin"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["redskin"])] = 1;
            }
            //12
            if (Request["greyskin"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["greyskin"])] = 1;
            }
            //13
            if (Request["chills"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["chills"])] = 1;
            }
            //14
            if (Request["cloudyeyes"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["cloudyeyes"])] = 1;
            }
            //15
            if (Request["coldeyes"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["coldeyes"])] = 1;
            }

            //16
            if (Request["scm"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["scm"])] = 1;
            }
            //17
            if (Request["conf"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["conf"])] = 1;
            }
            //18
            if (Request["const"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["const"])] = 1;
            }
            //19
            if (Request["muco"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["muco"])] = 1;
            }
            //20
            if (Request["drco"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["drco"])] = 1;
            }
            //21
            if (Request["cskin"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["cskin"])] = 1;
            }
            //22
            if (Request["du"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["du"])] = 1;
            }
            //23
            if (Request["dnonitem"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["dnonitem"])] = 1;
            }
            //24
            if (Request["dia"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["dia"])] = 1;
            }
            //25
            if (Request["dinlimbs"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["dinlimbs"])] = 1;
            }
            //26
            if (Request["diz"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["diz"])] = 1;
            }
            //27
            if (Request["drool"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["drool"])] = 1;
            }
            //28
            if (Request["deyes"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["deyes"])] = 1;
            }
            //29
            if (Request["dmouth"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["dmouth"])] = 1;
            }
            //30
            if (Request["dskin"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["dskin"])] = 1;
            }
            //31
            if (Request["eache"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["eache"])] = 1;
            }
            //32
            if (Request["bv"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["bv"])] = 1;
            }
            //33
            if (Request["fati"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["fati"])] = 1;
            }
            //34
            if (Request["fever"] != null)
            {
                symptomListDetector[Convert.ToInt32(Request["fever"])] = 1;
            }
            //35
            if (Request["flu"] != null)
            {
                symptomListDetector[Convert.ToInt32(Request["flu"])] = 1;
            }
            //36
            if (Request["fam"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["fam"])] = 1;
            }
            //37
            if (Request["fstools"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["fstools"])] = 1;
            }
            //38
            if (Request["furine"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["furine"])] = 1;
            }
            //39
            if (Request["spotoncheeks"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["spotoncheeks"])] = 1;
            }
            //40
            if (Request["hloss"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["hloss"])] = 1;
            }
            //41
            if (Request["hburn"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["hburn"])] = 1;
            }
            //42
            if (Request["hunger"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["hunger"])] = 1;
            }
            //43
            if (Request["infurine"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["infurine"])] = 1;
            }
            //44
            if (Request["insomnia"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["insomnia"])] = 1;
            }
            //45
            if (Request["redspots"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["redspots"])] = 1;
            }
            //46
            if (Request["blisters"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["blisters"])] = 1;
            }
            //47
            if (Request["rashes"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["rashes"])] = 1;
            }
            //48
            if (Request["ithroat"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["ithroat"])] = 1;
            }
            //49
            if (Request["lossappe"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["lossappe"])] = 1;
            }
            //50
            if (Request["chestlumps"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["chestlumps"])] = 1;
            }
            //51
            if (Request["eyelumps"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["eyelumps"])] = 1;
            }
            //52
            if (Request["bodylimps"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["bodylimps"])] = 1;
            }
            //53
            if (Request["nausea"] != null)
            {
                symptomListDetector[Convert.ToInt32(Request["nausea"])] = 1;
            }
            //54
            if (Request["nbleed"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["nbleed"])] = 1;
            }
            //55
            if (Request["headache"] != null)
            {
               // count++;
                symptomListDetector[Convert.ToInt32(Request["headache"])] = 1;
            }
            //56
            if (Request["stomache"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["stomache"])] = 1;
            }
            //57
            if (Request["backache"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["backache"])] = 1;
            }
            //58
            if (Request["abpain"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["abpain"])] = 1;
            }
            //59
            if (Request["jointpain"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["jointpain"])] = 1;
            }
            //60
            if (Request["musclecramps"] != null)
            {
               // count++;
                symptomListDetector[Convert.ToInt32(Request["musclecramps"])] = 1;
            }
            //61
            if (Request["thrpain"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["thrpain"])] = 1;
            }
            //62
            if (Request["shootpain"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["shootpain"])] = 1;
            }
            //63
            if (Request["shoulderpain"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["shoulderpain"])] = 1;
            }
            //64
            if (Request["eyepain"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["eyepain"])] = 1;
            }
            //65
            if (Request["chestpain"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["chestpain"])] = 1;
            }
            //66
            if (Request["rheartrate"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["rheartrate"])] = 1;
            }
            //67
            if (Request["seizure"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["seizure"])] = 1;
            }
            //68
            if (Request["sheartrate"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["sheartrate"])] = 1;
            }
            //69
            if (Request["sorethroat"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["sorethroat"])] = 1;
            }
            //70
            if (Request["stiffnes"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["stiffnes"])] = 1;
            }
            //71
            if (Request["sweating"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["sweating"])] = 1;
            }
            //72
            if (Request["neckswelling"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["neckswelling"])] = 1;
            }
            //73
            if (Request["tongueswelling"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["tongueswelling"])] = 1;
            }
            //74
            if (Request["jointsswelling"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["jointsswelling"])] = 1;
            }
            //75
            if (Request["armpitsswelling"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["armpitsswelling"])] = 1;
            }
            //76
            if (Request["faceswelling"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["faceswelling"])] = 1;
            }
            //77
            if (Request["stomachswelling"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["stomachswelling"])] = 1;
            }
            //78
            if (Request["skinswelling"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["skinswelling"])] = 1;
            }
            //79
            if (Request["feetswelling"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["feetswelling"])] = 1;
            }
            //80
            if (Request["lipsswelling"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["lipsswelling"])] = 1;
            }
            //81
            if (Request["handsswelling"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["handsswelling"])] = 1;
            }
            //82
            if (Request["legsswelling"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["legsswelling"])] = 1;
            }
            //83
            if (Request["thirst"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["thirst"])] = 1;
            }
            //84
            if (Request["vdisturbance"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["vdisturbance"])] = 1;
            }
            //85
            if (Request["vomit"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["vomit"])] = 1;
            }
            //86
            if (Request["weakness"] != null)
            {
                //count++;
                symptomListDetector[Convert.ToInt32(Request["weakness"])] = 1;
            }
            //87
            if (Request["weightgain"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["weightgain"])] = 1;
            }
            //88
            if (Request["weightloss"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["weightloss"])] = 1;
            }
            //89
            if (Request["wheezing"] != null)
            {
                count++;
                symptomListDetector[Convert.ToInt32(Request["wheezing"])] = 1;
            }
            if (count < 3)
            {
                ViewBag.Less = "not enough";
                return View();
            }
            int bre = 0;
            int [] diseaseID = new int[3];
            diseaseID = Models.Symptoms.diseaseDetector(symptomListDetector);

            //getting disease id 
            string[] diseaseName = new string[3];
            for (int i = 0; i < 3; i++)
            {
                diseaseName[i] = Models.Symptoms.getDiseaseByName(diseaseID[i]);
            }
            Session["d1"] = Convert.ToString( diseaseName[0]);
            Session["d2"] = Convert.ToString(diseaseName[1]);
            Session["d3"] = Convert.ToString(diseaseName[2]);

            Session["d1id"] = diseaseID[0];
            Session["d2id"] = diseaseID[1];
            Session["d3id"] = diseaseID[2];
            int b = 0;
            return Redirect("~/Symptoms/diseaseName");

           // int brea = 0;
            //getting list of specilizations under which this disease can be diagnosed
            int[] specializationList = new int[20];
           // specializationList = Models.Symptoms.getSpecializationList(diseaseID);

            //getting list of doctors under that specialization
            List<int> doctorsIDList = new List<int>();
            doctorsIDList = Models.User.getDoctorsIDList(specializationList);

            //getting info of doctors that can diagnose the disease
            List<Models.UserDTO> doctorsList = new List<Models.UserDTO>();
            doctorsList = Models.User.getDoctorsList(doctorsIDList);
            int x = 0;
            return Redirect("~/Symptoms/symptoms");

        }
        [HttpGet]
        public ActionResult doctors()
        {
            if (Session["user_id"] == null)
            {
                return Redirect("~/Home/etechcare");
            }
            int[] diseaseID = new int[3];
            diseaseID[0] = Convert.ToInt32(Session["d1id"]);
            diseaseID[1] = Convert.ToInt32(Session["d2id"]);
            diseaseID[2] = Convert.ToInt32(Session["d3id"]);
            int ii = 0;
            int[] specializationList = new int[20];
            for (int i = 0;i < 3;i++)
            {
                specializationList = new int[20];
                specializationList = Models.Symptoms.getSpecializationList(diseaseID[i]);
            }
            

            //getting list of doctors under that specialization
            List<int> doctorsIDList = new List<int>();
            doctorsIDList = Models.User.getDoctorsIDList(specializationList);

            //getting info of doctors that can diagnose the disease
            List<Models.UserDTO> doctorsList = new List<Models.UserDTO>();
            doctorsList = Models.User.getDoctorsList(doctorsIDList);
            int x = 0;
            return View(doctorsList);
        }

        [HttpPost]
        public ActionResult doctors(string dname)
        {
            if (Session["user_id"] == null)
            {
                return Redirect("~/Home/etechcare");
            }
            int diseaseID;
            diseaseID = Convert.ToInt32(dname);
            int ii = 0;
            int[] specializationList = new int[20];
           
            
            specializationList = new int[20];
            specializationList = Models.Symptoms.getSpecializationList(diseaseID);
            


            //getting list of doctors under that specialization
            List<int> doctorsIDList = new List<int>();
            doctorsIDList = Models.User.getDoctorsIDList(specializationList);

            //getting info of doctors that can diagnose the disease
            List<Models.UserDTO> doctorsList = new List<Models.UserDTO>();
            doctorsList = Models.User.getDoctorsList(doctorsIDList);
            int x = 0;
            return View(doctorsList);
        }
 
	}
}