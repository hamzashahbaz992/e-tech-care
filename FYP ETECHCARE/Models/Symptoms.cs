﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace ETechCare.Models
{
    public static class Symptoms
    {
        public static String _connStr = System.Configuration.ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;
        

        public static int[] diseaseDetector(int [] symptomsList)
        {
            int[] diseaseID = new int[3];
            int [] diseaseProbability = new int[73];
            for (int i = 0; i < 73;i++ )
            {
                diseaseProbability[i] = 0;
            }
                for (int i = 1; i < 73; i++)
                {
                    SqlConnection conn = new SqlConnection(_connStr);
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "Select * from disease_symtoms where did = '" + i + "'";
                    cmd.Connection = conn;
                    SqlDataReader rd = cmd.ExecuteReader();
                    int scorePercent = 0;
                    while (rd.Read())
                    {
                        int symptomID = Convert.ToInt32(rd["sid"].ToString());
                        if (symptomsList[symptomID] == 1)
                        {
                            scorePercent = scorePercent + Convert.ToInt32(rd["score"].ToString());
                        }

                    }
                    diseaseProbability[i] = scorePercent;


                }
            //getting 1D
            int m = diseaseProbability.Max();
            diseaseID[0] = Array.IndexOf(diseaseProbability, m);

            //getting 2
            diseaseProbability[diseaseID[0]] = 0;
            int m1 = diseaseProbability.Max();
            diseaseID[1] = Array.IndexOf(diseaseProbability, m1);

            //getting 3
            diseaseProbability[diseaseID[1]] = 0;
            int m2 = diseaseProbability.Max();
            diseaseID[2] = Array.IndexOf(diseaseProbability, m2);

            return diseaseID;
        }
        public static string getDiseaseByName(int did)
        {
            string diseaseName = "";
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "Select * from disease where did = '"+did+"' ";
            cmd.Connection = conn;
            SqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                diseaseName = rd["name"].ToString();
            }
            conn.Close();
            return diseaseName;
        }

       public static int[] getSpecializationList(int diseaseID)
       {
           int[] specializationList = new int[20];
           for (int i = 0; i < 20; i++)
           {
               specializationList[i] = 0;
           }
           SqlConnection conn = new SqlConnection(_connStr);
           conn.Open();
           SqlCommand cmd = new SqlCommand();
           cmd.CommandText = "Select * from disease_specialization where did = '" + diseaseID + "'";
           cmd.Connection = conn;
           SqlDataReader rd = cmd.ExecuteReader();
           while(rd.Read())
           {
               specializationList[Convert.ToInt32(rd["spid"].ToString())] = 1;
           }
           conn.Close();
           return specializationList;

       }
    
    }
}