﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETechCare.Models
{
    public class UserDTO
    {
        public int UserID { get; set; }
        public String Name { get; set; }
        public String email { get; set; }
        public String Password { get; set; }

        public String city { get; set; }
        public String signupas { get; set; }

        public String cemail { get; set; }
        public String fee { get; set; }
        public String number { get; set; }
        public String time { get; set; }
        public String hospital { get; set; }
        public String address { get; set; }
        public String specialization { get; set; }
        public String specializationID { get; set; }

        public String pic { get; set; }

    }
}