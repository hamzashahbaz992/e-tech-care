﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

using System.Net;
using System.Net.Mail;
namespace ETechCare.Models
{
    public static class User
    {
        public static String _connStr = System.Configuration.ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;
        public static bool validateUser(string email)
        {
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "Select * from users where email = '" + email + "'";
            cmd.Connection = conn;
            SqlDataReader rd = cmd.ExecuteReader();
            int count = 0;
            while (rd.Read())
            {
                count++;
                return false;
            }
            conn.Close();

            return true;
        }



        public static int login(string email, string password)
        {
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "Select * from users where email = '" + email + "' and password = '" + password + "'";
            cmd.Connection = conn;
            SqlDataReader rd = cmd.ExecuteReader();
            int count = 0;
            while (rd.Read())
            {
                string role = rd["signupas"].ToString();
                if (role == "Doctor")
                {
                    return 2;
                }
                else if (role == "Patient")
                {
                    return 1;
                }
                count++;
            }
            conn.Close();

            return 0;
        }

        public static bool signUp(UserDTO user)
        {
            if (validateUser(user.email) == false)
            {
                return false;
            }
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            string q = "INSERT INTO users(name,password,email,city,signupas) VALUES('" + user.Name + "','" + user.Password + "','" + user.email + "','" + user.city + "','" + user.signupas + "')";
            SqlCommand cmd = new SqlCommand(q, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
            return true;
        }
        public static bool addInfo(string description,int uid)
        {
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            string query = "UPDATE users Set  description ='" + description + "'  where id = '" + uid + "'";
            //string q = "INSERT INTO test2(description) VALUES('" + description + "')";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
           // conn.Close();
            return true;
        }
        public static int getID(string email)
        {
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "Select * from users where email = '" + email + "'";
            cmd.Connection = conn;
            SqlDataReader rd = cmd.ExecuteReader();
            int id = 0;
            int count = 0;
            while (rd.Read())
            {
                id = Convert.ToInt32( rd["id"].ToString());
                count++;
                return id;
            }
            conn.Close();
            return id;
        }

        public static bool addDoctorSpecialization(int did,int spid)
        {
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            string q = "INSERT INTO doctor_specialization(did,spid) VALUES('" + did + "','" + spid + "')";
            SqlCommand cmd = new SqlCommand(q, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
            return true;
        }

        public static List<int> getDoctorsIDList(int[] specialiationList)
        {
            List<int> doctorsList = new List<int>();
            for (int i = 1; i < specialiationList.Length; i++)
            {
                if (specialiationList[i] == 1)
                {
                    SqlConnection conn = new SqlConnection(_connStr);
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "Select * from doctor_specialization where spid = '" + i + "'";
                    cmd.Connection = conn;
                    SqlDataReader rd = cmd.ExecuteReader();
                    int j = 0;
                    while (rd.Read() && j == 0)
                    {
                        doctorsList.Add(Convert.ToInt32(rd["did"].ToString()));
                        j++;
                    }
                    conn.Close();
                }
            }

            return doctorsList;
        }
        public static int getSpecializationID(int did)
        {
            int id = 0;
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "Select * from doctor_specialization where did = '" + did + "' ";
            cmd.Connection = conn;
            SqlDataReader rd = cmd.ExecuteReader();
            while  (rd.Read())
            {
                id = Convert.ToInt32( rd["spid"].ToString());
            }
            conn.Close();
            return id;
        }
        public static String getSpecializationName(int spid)
        {
            String name = "";
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "Select name from specialization where spid = '" + spid + "' ";
            cmd.Connection = conn;
            SqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                name =rd["name"].ToString();
            }
            conn.Close();
            return name;
        }
        public static UserDTO getUserByID(int id)
        {
            UserDTO user = new UserDTO();
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "Select * from users where id = '" + id + "' ";
            cmd.Connection = conn;
            SqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                user.UserID = Convert.ToInt32( rd["id"].ToString());
                user.city = rd["city"].ToString();
                user.email = rd["email"].ToString();
                user.Name = rd["name"].ToString();
                user.Password = rd["password"].ToString();
                user.pic = rd["pic"].ToString();
                string description = rd["description"].ToString();
                string[] des = description.Split('|');
                user.cemail = des[0];
                user.fee = des[1];
                user.hospital = des[2];
                user.address = des[3];
                user.time = des[4];
                user.number = des[5];

            }
            int spid = getSpecializationID(id);
            user.specializationID = Convert.ToString(spid);
            user.specialization = getSpecializationName(spid);
            conn.Close();
            return user;
        }
        public static List<UserDTO> getDoctorsList(List<int> userID)
        {
            List<UserDTO> doctorsList = new List<UserDTO>();
            foreach (int a in userID)
            {
                doctorsList.Add(getUserByID(a));
            }
            return doctorsList;
        }
        public static bool updateInfo(string description, int uid,string name,string password,string city)
        {
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            string query = "UPDATE users Set  description ='" + description + "',name = '"+name+"',password = '" + password +"',city = '"+city+"'  where id = '" + uid + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
            return true;
        }
        public static bool uploadpic(string pic,int uid)
        {
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            string query = "UPDATE users Set pic = '" + pic + "'  where id = '" + uid + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
            return true;
        }
        public static Boolean mail(String email)
        {
            if (validateUser(email) != false)
            {
                return false;
            }
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            string message = "Your new password is ";
            try
            {
                message = message + "1234";
                MailMessage msg = new MailMessage("testmail5134@gmail.com", email, "New Password", message);
                msg.IsBodyHtml = true;
                SmtpClient sc = new SmtpClient("smtp.gmail.com", 587);
                sc.UseDefaultCredentials = false;
                NetworkCredential cre = new NetworkCredential("testmail5134@gmail.com", "12345678test");
                sc.Credentials = cre;
                sc.EnableSsl = true;
                sc.Send(msg);
                int uid = getID(email);
                int pass = 1234;
                string query = "UPDATE users Set password = '" + pass + "'  where id = '" + uid + "'";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}